# zvonok_test

1. Для решения данной задачи, я использовал LEFT JOIN, который объединяет таблицы слева, и выбираю те статьи, у которых нет связанных комментариев.

```sql
SELECT article.*
FROM article
LEFT JOIN comment ON article.id = comment.article_id
WHERE comment.id IS NULL;
```
2. Для решения этой задачи, я использую словарь, где ключом будет имя работника, а значением - список с его часами. Также с помощью регулярных выражений, разделяю каждую строку на имя и значение часов с помощью метода re.match.

```python
import re

worker_dict = {}

with open('input.txt') as f:
    for line in f:
        match = re.match(r'(\w.+)\s(\d+)', line)
        if match:
            name, value = match.group(1), match.group(2)
            if name in worker_dict:
                worker_dict[name]['hours'].append(int(value))
            else:
                worker_dict[name] = {'hours': [int(value)]}

for worker, data in worker_dict.items():
    worker_hours = data['hours']
    worker_hours_str = ', '.join(map(str, data['hours']))
    print(f"{worker}: {worker_hours_str}; sum: {sum(worker_hours)}")
```
input.txt и исходный код в файле также продублировал в корневом каталоге проекта.
