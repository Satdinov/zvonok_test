import re

worker_dict = {}

with open('input.txt') as f:
    for line in f:
        match = re.match(r'(\w.+)\s(\d+)', line)
        if match:
            name, value = match.group(1), match.group(2)
            if name in worker_dict:
                worker_dict[name]['hours'].append(int(value))
            else:
                worker_dict[name] = {'hours': [int(value)]}

for worker, data in worker_dict.items():
    worker_hours = data['hours']
    worker_hours_str = ', '.join(map(str, data['hours']))
    print(f"{worker}: {worker_hours_str}; sum: {sum(worker_hours)}")
